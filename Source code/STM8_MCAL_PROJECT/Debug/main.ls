   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Parser V4.11.9 - 08 Feb 2017
   3                     ; Generator (Limited) V4.4.6 - 08 Feb 2017
  43                     ; 10 main()
  43                     ; 11 {
  45                     	switch	.text
  46  0000               _main:
  50                     ; 12 	GPIOA->DDR = 0x08;
  52  0000 35085002      	mov	20482,#8
  53                     ; 13 	GPIOA->CR1 = 0x08;
  55  0004 35085003      	mov	20483,#8
  56  0008               L12:
  57                     ; 17 	for(i=0;i<5000;i++);
  59  0008 5f            	clrw	x
  60  0009 bf00          	ldw	_i,x
  61  000b               L52:
  65  000b be00          	ldw	x,_i
  66  000d 1c0001        	addw	x,#1
  67  0010 bf00          	ldw	_i,x
  70  0012 be00          	ldw	x,_i
  71  0014 a31388        	cpw	x,#5000
  72  0017 25f2          	jrult	L52
  73                     ; 18   GPIOA->ODR ^= 0x08;	
  75  0019 90165000      	bcpl	20480,#3
  77  001d 20e9          	jra	L12
 101                     	xdef	_main
 102                     	switch	.ubsct
 103  0000               _i:
 104  0000 0000          	ds.b	2
 105                     	xdef	_i
 125                     	end
